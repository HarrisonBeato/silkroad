package com.devoreo.silkroad;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class MainActivity extends AppCompatActivity {

    Button button;
    TextView nomeProduto, vendedor, descricao, preco;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button = findViewById(R.id.btnGet);
        nomeProduto = findViewById(R.id.txtNome);
        vendedor = findViewById(R.id.txtVend);
        preco = findViewById(R.id.txtPreco);
        descricao = findViewById(R.id.txtDesc);
        //pega a url base da api e a partir desse arquivo concatenamos os endpoints
        String URL = "https://proejtonoismandanisso.firebaseio.com/produto.json";


        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //cria um objeto para requisição do tipo JSON.

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL,
                //obj q quero mandar como o header
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            nomeProduto.setText(response.getString("nomeproduto"));
                            vendedor.setText(response.getString("vendedor"));
                            preco.setText(response.getString("preco"));
                            descricao.setText(response.getString("descricao"));

                            Iterator keys = response.keys();
                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                // get the value of the dynamic key
                                //
                                JSONObject currentDynamicValue = response.getJSONObject(currentDynamicKey);
                                Log.i("Nome Produto: ", currentDynamicValue.getString("nomeproduto"));
                                Log.i("Preço Produto: ", currentDynamicValue.getString("preco"));
                                Log.i("Descrição Produto: ", currentDynamicValue.getString("descricao"));
                                Log.i("Vendedor: ", currentDynamicValue.getString("vendedor"));

                            }

                            //}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Request Fail: ", error.toString());

                        Toast.makeText(MainActivity.this, "Não existem dados para listar", Toast.LENGTH_SHORT).show();

                    }
                }


        );
        MySingleton.getInstance(MainActivity.this).addToRequestque(jsonObjectRequest);
        //requestQueue.add(jsonObjectRequest);
    }
}
