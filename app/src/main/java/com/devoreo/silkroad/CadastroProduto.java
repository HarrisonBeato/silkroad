package com.devoreo.silkroad;

public class CadastroProduto {
    private String nomeUsuario;
    private String nomeProduto;
    private String valorProduto;
    private String descricaoProduto;
    private int imagemProduto;

    CadastroProduto(){

    }

    public String getNomeUsuario(){
        return nomeUsuario;
    }

    public void setNomeProduto(String nomeProduto){
        this.nomeProduto = nomeProduto;
    }

    public String getNomeProduto(){
        return nomeProduto;
    }

    public void setNomeUsuario(String nomeUsuario){
        this.nomeProduto = nomeProduto;
    }

    public String getValorProduto(){
        return valorProduto;
    }

    public void setValorProduto(String valorProduto){
        this.valorProduto = valorProduto;
    }

    public String getDescricaoProduto(){
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto){
        this.descricaoProduto = descricaoProduto;
    }

    public int getImagemProduto(){
        return imagemProduto;
    }

    public void setImagemProduto(int imagemProduto){
        this.imagemProduto = imagemProduto;
    }





}
